<?php

namespace App\Providers;

use App\Models\Categoria;
use App\Models\Proveedor;
use App\Models\Stock;
use App\Models\StockAudit;
use App\Models\StockStatus;
use App\Models\Subcategoria1;
use App\Models\Subcategoria2;
use App\Models\Tienda;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\SomeEvent' => [
            'App\Listeners\EventListener',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        $events->listen('eloquent.saved: *', function ($model) {
            if($model instanceof Stock && ($user_id = Auth::id()) ){
                $this->registrarActividad($model, $user_id);
            }
        });
    }

    /**
     * @param $stock
     * @param $user_id
     */
    private function registrarActividad($stock, $user_id)
    {
        $stockAudit = new StockAudit();
        $stockAudit->stock_id = $stock->id;
        $stockAudit->categoria_id = $stock->categoria_id ? Categoria::find($stock->categoria_id)->toJson() : "";
        $stockAudit->new_status = $stock->status ? StockStatus::find($stock->status)->toJson() : "";
        $stockAudit->subcategoria1_id = $stock->subcategoria1_id ? Subcategoria1::find($stock->subcategoria1_id)->toJson() : "";
        $stockAudit->subcategoria2_id = $stock->subcategoria2_id ? Subcategoria2::find($stock->subcategoria2_id)->toJson() : "";
        $stockAudit->tienda_id = $stock->tienda_id ? Tienda::find($stock->tienda_id)->toJson() : "";
        $stockAudit->proveedor_id = $stock->proveedor_id ? Proveedor::find($stock->proveedor_id)->toJson() : "";
        $stockAudit->cantidad = $stock->cantidad;
        $stockAudit->precio = $stock->precio;
        $stockAudit->numero_factura = $stock->numero_factura;
        $stockAudit->vida_util = $stock->vida_util;
        $stockAudit->fecha_adquisicion = $stock->fecha_adquisicion;
        $stockAudit->codigo = $stock->codigo;
        $stockAudit->codigo_gerp = $stock->codigo_gerp;
        $stockAudit->user = $user_id ? User::find($user_id)->toJson() : "";
        $stockAudit->date = $stock->updated_at;

        $stockAudit->save();

    }
}
