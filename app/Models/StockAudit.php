<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockAudit extends Model
{
    protected $table = 'stock_audit';

    public $timestamps = false;

    protected $fillable = [
        'cantidad',
        'categoria_id',
        'codigo',
        'codigo_gerp',
        'date',
        'fecha_adquisicion',
        'numero_factura',
        'subcategoria1_id',
        'subcategoria2_id',
        'new_status',
        'old_status',
        'precio',
        'tienda_id',
        'vida_util',
        'user'
    ];

}
