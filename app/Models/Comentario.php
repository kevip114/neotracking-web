<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    protected $table = 'comentarios';

    protected $fillable = [
        'stock_id',
        'user_id',
        'mensaje',
        'created_at',
        'updated_at'
    ];

    public function usuario(){
        return $this->belongsTo(User::class, 'user_id');
    }
    public function stock(){
        return $this->belongsTo(Stock::class, 'stock_id');
    }
}
