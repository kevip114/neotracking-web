<?php

namespace App\Repositories;

use App\Models\DireccionUbicacion;
use App\Models\Ubicacion;
use Symfony\Component\HttpFoundation\Request;

use App\Models\Tienda;

class TiendasRepository {

    const ALTA = 1;
    const BAJA = 2;
    const PENDIENTE_ALTA = 3;
    const PENDIENTE_BAJA = 4;
    const PENDIENTE_ALTA_PUEDE_EDITAR = 5;

    /**
     * @param $id
     * @return Tienda
     */
    public function find($id){
        return Tienda::find($id);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request){

        $u = $request->ubicacion;
        $ub = DireccionUbicacion::create([
           'distrito_id' => $u['distrito_id'],
           'ciudad_id' => $u['ciudad_id'],
           'region1_id'  => $u['region1'],
           'region2_id'  => $u['region2'],
           'provincia_id' => $u['provincia_id'],
           'departamento_id' => $u['departamento_id']
        ]);

        $tienda = Tienda::create([
            'channel_id' => $request->channel_id,
            'retail_id' => $request->retail_id,
            'tipo_tienda_id'  => $request->tipo_tienda_id,
            'direccion'  =>  $request->direccion,
            'name' => strtoupper($request->name),
            'direccion_ubicacion_id' => $ub->id
        ]);

        return $tienda->id;
    }
    /**
     * Muestra una coleccion de tiendas sin stock
     * @return Collection
     */
    public function getTiendasConStock(Request $request)
    {
        $query = $this->buildQueryTiendasConStock();
        $muebles = collect($query->get());
        if(isset($request->page)){
            return $this->paginate($request->pagination, $request->page, $muebles);
        }
        return $muebles;
    }

    /**
     * Muestra una coleccion de tiendas sin stock
     * @return Collection
     */
    public function getTiendasSinStock(Request $request)
    {
        $query = $this->buildQueryTiendasSinStock();
        $muebles = collect($query->get());
        if(isset($request->page)){
            return $this->paginate($request->pagination, $request->page, $muebles);
        }
        return $muebles;
    }

    private function buildQueryTiendasConStock()
    {
        $tiendasConStock = \DB::table("stock")
            ->where("stock.status","=", self::ALTA)
            ->orWhere("stock.status","=", self::PENDIENTE_BAJA)
            ->groupBy("tienda_id")->get();
        $query = \DB::table("tienda");
        $query = $this->selectTiendas($query);
        $query = $query->join("retail", "retail.id", "=", "tienda.retail_id")
            ->whereIn("tienda.id", collect($tiendasConStock)->pluck("tienda_id"))
            ->orderBy("tienda.name");
        return $query;
    }

    private function buildQueryTiendasSinStock()
    {
        $tiendasConStock = \DB::table("stock")
                ->where("stock.status","=", self::ALTA)
                ->orWhere("stock.status","=", self::PENDIENTE_BAJA)
                ->groupBy("tienda_id")->get();
        $query = \DB::table("tienda");
        $query = $this->selectTiendas($query);
        $query = $query->join("retail", "retail.id", "=", "tienda.retail_id")
                ->whereNotIn("tienda.id", collect($tiendasConStock)->pluck("tienda_id"))
                ->orderBy("tienda.name");
        return $query;
    }

    private function selectTiendas($query)
    {
        $query = $query->select([
            'tienda.id',
            'tienda.name as tienda',
            'tienda.direccion as direccion',
            'retail.name as retail'
        ]);

        return $query;
    }

    private function paginate($pagination=null, $page, $muebles)
    {
        $pagination = isset($pagination) ? $pagination : 20;
        $prev_page = ($page-1 <= 0) ? null : $page-1;
        $next_page = (count($muebles->forPage($page+1,$pagination))) ? $page+1 : null;

        $response = new \StdClass();

        $response->prev_page = $prev_page;
        $response->current_page = intval($page);
        $response->next_page = $next_page;
        $response->data = $muebles->forPage($page,$pagination);
        $response->per_page = $pagination;
        $response->total = count($muebles);
        return collect($response);
    }


}