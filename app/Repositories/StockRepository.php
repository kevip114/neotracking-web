<?php

namespace App\Repositories;

use App\Models\Proveedor;
use App\Models\StockAudit;
use App\Models\Track;
use App\Models\TrackImagen;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\Request;

use App\Models\Stock;
use App\User;

class StockRepository
{
    const STATUS_ALTA = 1;
    const STATUS_BAJA = 2;
    const STATUS_PENDIENTE_ALTA = 3;
    const STATUS_PENDIENTE_BAJA = 4;
    const STATUS_PENDIENTE_ALTA_PUEDE_EDITAR = 5;

    public function baja(Request $request, $codigo){
        return $this->cambiarEstado(self::STATUS_BAJA, $codigo);
    }


    public function store(Request $request){
        $validator = \Validator::make($request->all(), [
            'codigo' => 'required|unique:stock',
            'cantidad' =>'required|numeric',
            'subcategoria1' =>'required',
            'subcategoria2' =>'required',
            'categoria' =>'required',
            'tipo_stock' =>'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 428);
        }

        $stock = new Stock();
        $stock->codigo = $request->codigo;
        $stock->subcategoria1_id = $request->subcategoria1;
        $stock->subcategoria2_id = $request->subcategoria2;
        $stock->categoria_id = $request->categoria;
        $stock->tipo_stock = $request->tipo_stock;
        $stock->cantidad = $request->cantidad;
        $stock->status = $this->STOCK_STATUS['pendiente'];
        $stock->save();
        return $stock;
    }
    public function index(Request $request){

        $query1 = \DB::table("stock");
        $query1 = $query1
            ->join("tienda","tienda.id","=","stock.tienda_id")
            ->join("stock_status","stock_status.id","=","stock.status")
            ->where("stock_status.name", "=", "pendiente_alta")
            ->orWhere("stock_status.name", "=", "pendiente_alta_puede_editar");

        if(isset($request->keyword)){
            $query1 = $query1->where('tienda.name', "like", "%" . $request->keyword . "%")
                        ->orWhere('stock.codigo', "like", "%" . $request->keyword . "%");
        }

        $query1 = $query1->select(
                "stock.id",
                "stock.codigo",
                "tienda.name as tienda",
                "stock_status.name as status"
            )
            ->orderBy('stock_status.name', 'desc');


        $query1 = $query1->get();

        $query2 = \DB::table("stock");
        $query2 = $query2
            ->join("categoria","categoria.id","=","stock.categoria_id")
            ->join("subcategoria1","subcategoria1.id","=","stock.subcategoria1_id")
            ->join("subcategoria2","subcategoria2.id","=","stock.subcategoria2_id")
            ->join("tienda","tienda.id","=","stock.tienda_id")
            ->join("stock_status","stock_status.id","=","stock.status")
            ->where("stock_status.name", "!=", "baja")
            ->where("stock_status.name", "!=", "pendiente_alta")
            ->where("stock_status.name", "!=", "pendiente_alta_puede_editar");

        if(isset($request->keyword)){
            $query2 = $query2->where('tienda.name', "like", "%" . $request->keyword . "%")
                ->orWhere('stock.codigo', "like", "%" . $request->keyword . "%")
                ->orWhere('categoria.tipo', "like", "%" . $request->keyword . "%")
                ->orWhere('subcategoria1.tipo', "like", "%" . $request->keyword . "%")
                ->orWhere('subcategoria2.tipo', "like", "%" . $request->keyword . "%");
        }

        $query2 = $query2->select(
                "stock.id",
                "stock.codigo",
                "categoria.tipo as categoria",
                "subcategoria1.tipo as subcategoria1",
                "subcategoria2.tipo as subcategoria2",
                "tienda.name as tienda",
                "stock_status.name as status"
            )
            ->orderBy('stock_status.name', 'desc');
        $query2 = $query2->get();

        $stock = collect(array_merge($query1, $query2));

        if(isset($request->page)){
            $page = $request->page;
            $pagination = isset($request->pagination) ? $request->pagination : 20;
            $prev_page = ($page-1 <= 0) ? null : $page-1;
            $next_page = (count($stock->forPage($page+1,$pagination))) ? $page+1 : null;

            $response = new \StdClass();

            $response->prev_page = $prev_page;
            $response->current_page = intval($page);
            $response->next_page = $next_page;
            $response->data = $stock->forPage($page,$pagination);
            $response->per_page = $pagination;
            $response->total = count($stock);
            return collect($response);
        }

        return array_merge($query1, $query2);
    }

    public function getHistory(Request $request)
    {
        $stockAudit = StockAudit::orderBy("date", "desc")->get();
        if(isset($request->page)){
            $page = $request->page;
            $pagination = isset($request->pagination) ? $request->pagination : 10;
            $prev_page = ($page-1 <= 0) ? null : $page-1;
            $next_page = (count($stockAudit->forPage($page+1,$pagination))) ? $page+1 : null;

            $response = new \StdClass();

            $response->prev_page = $prev_page;
            $response->current_page = intval($page);
            $response->next_page = $next_page;
            $response->data = $stockAudit->forPage($page,$pagination);
            $response->per_page = $pagination;
            $response->total = count($stockAudit);
            return collect($response);
        }
        return $stockAudit;
    }

    /**
     * Actualiza los datos del mobiliario, si el mobiliario está en estado pendiente_alta lo cambia a pendiendte_alta_puede_editar
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        $stock = Stock::with(['tracking.trackImagen'])->find($id);
        $messages = [
            'required'=> ':attribute es requerido',
            'size'    => ':attribute debe tener una longitud de :size.',
            'numeric' => ':attribute debe contener solo numeros',
            'unique'  => ':attribute ya existe',
        ];
        $validator = \Validator::make($request->all(), [
            'categoria_id' => 'required | integer | min:1',
            'subcategoria1_id' => 'required | integer | min:1',
            'subcategoria2_id' => 'required | integer | min:1',
            'codigo_gerp'      => 'unique:stock,codigo_gerp,' . $stock->id,
            'numero_factura' => 'string:10 |numeric',
            'sugerencia_baja_antes_de_alta' => 'numeric | min: 0 | max: 1',
            'precio'        => 'regex:/^\d*(\.\d{1,2})?$/ | max: 9999999999.99'
        ],$messages);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 428);
        }
        if($stock->status == self::STATUS_PENDIENTE_ALTA){
            return response()->json(['errors' => 'debe dar de alta al usuario'], 428);
        }

        $this->setStockRequest($stock, $request);

        //solo en este caso se da de alta al mobiliario
        if($stock->status == self::STATUS_PENDIENTE_ALTA_PUEDE_EDITAR) {
            $query = \DB::table("track");
            $query = $query
                ->where('track.codigo',$stock->codigo)
                ->where('track_status.name','alta')
                ->join("track_status","track_status.id","=","track.status_id")
                ->join("tienda", "tienda.id", "=", "track.tienda_id")
                ->select("tienda.id")
                ->orderBy('track.created_at','desc');
            $tienda = $query->first();

            if($tienda) {
                $stock->tienda_id = $tienda->id;
            }
            if($request->sugerencia_baja_antes_de_alta == 1){
                $stock->status = self::STATUS_BAJA;
            } else {
                $stock->status = self::STATUS_ALTA;
            }
            //redimensiona imagenes
            $path = public_path() . '/images/resized-images';
            foreach($stock->tracking as $track){
                foreach($track->trackImagen as $track_imagen){
                    $pathImage = public_path() . '/images/' . $track_imagen->name;
                    $img = Image::make($pathImage);
                    if(!file_exists($path . '/60h' . $track_imagen->name)) {
                        $img->resize(null, 60, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                        $img->save($path . '/60h' . $track_imagen->name, 60);
                    }
                }
            }
        }
        $stock->save();

        return $stock;
    }
    /**
     * @param $id
     * @return Stock
     */
    public function find($id)
    {
        return Stock::with('roles')->find($id);
    }

    private function cambiarEstado($status, $codigo){
        $user_id = \Auth::user()->id;
        $user = User::with('roles')->find($user_id);
        $authorized = false;
        $stock = Stock::where('codigo',$codigo)->first();
        if (!is_null($user)) {
            foreach ($user->roles as $key => $role) {
                if ($role->name == "Administrador") {
                    $authorized = true;
                }
            }
            if($authorized){
                $stock->status = $status;
                $stock->save();
            }
        }

        return $stock;
    }

    private function setStockRequest(&$stock, $request)
    {
        $categoria = $request->categoria_id;
        if(!empty($categoria)){
            $stock->categoria_id = $categoria;
        }

        $subcategoria1 = $request->subcategoria1_id;
        if(!empty($subcategoria1)){
            $stock->subcategoria1_id = $subcategoria1;
        }

        $subcategoria2 = $request->subcategoria2_id;
        if(!empty($subcategoria2)){
            $stock->subcategoria2_id = $subcategoria2;
        }

        $precio = $request->precio;
        if(!empty($precio)){
            $stock->precio = $precio;
        }

        $numero_factura = $request->numero_factura;
        if(!empty($numero_factura)){
            $stock->numero_factura = $numero_factura;
        }

        $codigo_gerp = $request->codigo_gerp;
        if(!empty($codigo_gerp)){
            $stock->codigo_gerp = $codigo_gerp;
        }

        $vida_util = $request->vida_util;
        if(!empty($vida_util)){
            $stock->vida_util = $vida_util;
        }
        $fecha_adquisicion = $request->fecha_adquisicion;
        if(!empty($fecha_adquisicion)){
            $stock->fecha_adquisicion = new Carbon($fecha_adquisicion);
        }

        $sugerencia_baja_antes_de_alta= $request->sugerencia_baja_antes_de_alta;
        if(!empty($sugerencia_baja_antes_de_alta)){
            $stock->sugerencia_baja_antes_de_alta = $sugerencia_baja_antes_de_alta;
        }

        $status= $request->status;
        if(!empty($status)){
            $stock->status = $status;
        }

        //se añade comentario en tabla intermedia
        $comentario = $request->comentario;
        if(!empty($comentario)){
            $stock->comentariosUsuario()->attach(\Auth::user()->id,['mensaje'=>$comentario]);
        }

        $proveedor = $request->proveedor;
        if($proveedor){
            if(isset($proveedor['id'])){
                $stock->proveedor_id = $proveedor['id'];
            }else{
                $proveedor = strtoupper($proveedor);
                $p = Proveedor::firstOrCreate(["nombre"=>$proveedor]);
                $stock->proveedor_id = $p->id;
            }
        }else if($proveedor==""){
            //si el campo proveedor esta en blanco
            $stock->proveedor_id = null;
        }
    }

}