<?php

namespace App\Repositories\reportes;

use App\Models\Provincia;
use App\Models\Region1;
use App\Models\Retail;
use App\Models\Subcategoria1;
use App\Models\Subcategoria2;
use App\Models\Tienda;
use Illuminate\Support\Collection;

class ReporteMueblesRepository
{
    const ALTA = 1;
    const BAJA = 2;
    const PENDIENTE_ALTA = 3;
    const PENDIENTE_BAJA = 4;
    const PENDIENTE_ALTA_PUEDE_EDITAR = 5;

    /**
     * Este método retorna el total de muebles por cliente(retail)
     * @param $from
     * @param $to
     * @param $region1
     * @param null $status
     * @return mixed
     */
    public function getMueblesPorCliente($from, $to, $status = null, $region1 = null)
    {
        //@TODO generar query por fechas cuando el cliente lo requiera
        $select = [
            "retail.name as retail",
            \DB::raw("count(*) as total_muebles")
        ];
        $groupBy = ["retail.id"];
        $query = \DB::table("retail")
                ->select($select)
                ->join("tienda", "tienda.retail_id", "=", "retail.id")
                ->join("stock", "stock.tienda_id", "=", "tienda.id");

        if(isset($region1)){
            $query = $query->join("direccion_ubicacion", "direccion_ubicacion.id", "=", "tienda.direccion_ubicacion_id");
            if($region1 == "lima"){
                $query = $query->where("direccion_ubicacion.region1_id", "=", 1);//region lima
            }else if($region1 == "provincia"){
                $query = $query->where("direccion_ubicacion.region1_id", "=", 2);//region provincia
            }
        }
        if(isset($status)){
            if($status == "alta"){
                $query = $query->where("status", "=", self::ALTA);
                $query = $query->orWhere("status", "=", self::PENDIENTE_BAJA);
            }else if($status == "baja") {
                $query = $query->where("status", "=", self::BAJA);
            }else if($status == "pendiente_alta") {
                $query = $query->where("status", "=", self::PENDIENTE_ALTA)
                                ->orWhere("status", "=", self::PENDIENTE_ALTA_PUEDE_EDITAR);
            }else if($status == "pendiente_baja") {
                $query = $query->where("status", "=", self::PENDIENTE_BAJA);
            }else{
                $query = $query
                    ->where("status", "=", self::ALTA)
                    ->orWhere("status", "=", self::PENDIENTE_BAJA);
            }
        }else{
            $query = $query
                ->where("status", "=", self::ALTA)
                ->orWhere("status", "=", self::PENDIENTE_BAJA);
        }
        $query = $query->groupBy($groupBy);

        $muebles = $query->get();
        $muebles = $this->formatMueblesPorCliente($muebles);
        return $muebles;
    }

    /**
     * @param $from
     * @param $to
     * @param $categoria
     * @param null $status
     * @return null
     */
    public function getCategorias($from, $to, $categoria, $status = null)
    {
        $query = \DB::table("stock");
        $query = $this->selectCategorias($query);

        $muebles = $this->buildQueryCategorias($from, $to, $query)->get();
        $muebles = $this->formatCategories($muebles);
        return $muebles;
    }

    public function getSubCategorias($from, $to, $subcategoria, $status = null)
    {
        $query = \DB::table("stock");
        $query = $this->selectSubCategorias($query);

        $muebles = $this->buildQuerySubCategorias($from, $to, $query)->get();
        $muebles = $this->formatSubCategories($muebles);
        return $muebles;
    }

    public function getProvincias($from, $to, $status = null, $provincias = null)
    {
        $query = \DB::table("stock");
        $query = $this->selectProvincias($query);

        $lsProvincias = explode(',', $provincias);

        $muebles = $this->buildQueryProvincias($from, $to, $query, $lsProvincias)->get();
        $muebles = $this->formatProvincias($muebles);
        if($provincias !== "" && $provincias !== null) {
            if (count($provincias) > 0) {
                return $muebles->whereIn("provincia", $lsProvincias)->values();
            }
        }
        return $muebles;
    }

    public function getRegion1($from, $to, $status = null)
    {
        $query = \DB::table("stock");
        $query = $this->selectRegion1($query);

        $muebles = $this->buildQueryRegion1($from, $to, $query)->get();
        $muebles = $this->formatRegion($muebles);
        return $muebles;
    }

    public function getRegion2($from, $to, $status = null)
    {
        $query = \DB::table("stock");
        $query = $this->selectRegion2($query);

        $muebles = $this->buildQueryRegion2($from, $to, $query)->get();
        $muebles = $this->formatRegion($muebles);
        return $muebles;
    }

    private function buildQueryRegion1($from, $to, $query)
    {
        $query = $query->join("tienda", "tienda.id", "=", "stock.tienda_id")
            ->join("direccion_ubicacion", "direccion_ubicacion.id", "=", "tienda.direccion_ubicacion_id")
            ->join("region1", "region1.id", "=", "direccion_ubicacion.region1_id")
            ->where("status", "=", self::ALTA)
            ->orWhere("status", "=", self::PENDIENTE_BAJA)
            ->groupBy("region1.nombre");
        return $query;
    }

    private function buildQueryRegion2($from, $to, $query)
    {
        $query = $query->join("tienda", "tienda.id", "=", "stock.tienda_id")
            ->join("direccion_ubicacion", "direccion_ubicacion.id", "=", "tienda.direccion_ubicacion_id")
            ->join("region2", "region2.id", "=", "direccion_ubicacion.region2_id")
            ->where("status", "=", self::ALTA)
            ->orWhere("status", "=", self::PENDIENTE_BAJA)
            ->groupBy("region2.nombre");
        return $query;
    }

    private function buildQuerySubCategorias($from, $to, $query)
    {
        $query = $query->join("tienda", "tienda.id", "=", "stock.tienda_id")
            ->join("direccion_ubicacion", "direccion_ubicacion.id", "=", "tienda.direccion_ubicacion_id")
            ->join("subcategoria2", "subcategoria2.id", "=", "stock.subcategoria2_id")
            ->join("region1", "region1.id", "=", "direccion_ubicacion.region1_id")
            ->where("status", "=", self::ALTA)
            ->orWhere("status", "=", self::PENDIENTE_BAJA)
            ->groupBy("stock.subcategoria2_id");
        return $query;
    }

    private function buildQueryCategorias($from, $to, $query)
    {
        //@TODO generar query por fechas cuando el cliente lo requiera
        $query = $query->join("tienda", "tienda.id", "=", "stock.tienda_id")
            ->join("direccion_ubicacion", "direccion_ubicacion.id", "=", "tienda.direccion_ubicacion_id")
            ->join("subcategoria1", "subcategoria1.id", "=", "stock.subcategoria1_id")
            ->join("region1", "region1.id", "=", "direccion_ubicacion.region1_id")
            ->where("status", "=", self::ALTA)
            ->orWhere("status", "=", self::PENDIENTE_BAJA)
            ->groupBy(["stock.subcategoria1_id", "region1.nombre"]);
        return $query;
    }

    private function buildQueryProvincias($from, $to, $query)
    {
        //@TODO generar query por fechas cuando el cliente lo requiera
        $query = $query->join("tienda", "tienda.id", "=", "stock.tienda_id")
            ->join("direccion_ubicacion", "direccion_ubicacion.id", "=", "tienda.direccion_ubicacion_id")
            ->join("provincia", "provincia.id", "=", "direccion_ubicacion.provincia_id")
            ->where("status", "=", self::ALTA)
            ->orWhere("status", "=", self::PENDIENTE_BAJA)
            ->groupBy(["provincia.nombre"]);
        return $query;
    }

    private function selectCategorias($query)
    {
        $query = $query->select([
            'region1.nombre as region',
            \DB::raw("count(subcategoria1_id) as cantidad"),
            'subcategoria1.tipo as categoria'
            ]);

        return $query;
    }

    private function selectRegion1($query)
    {
        $query = $query->select([
            'region1.nombre as region1',
            \DB::raw("count(stock.id) as cantidad")
            ]);

        return $query;
    }

    private function selectRegion2($query)
    {
        $query = $query->select([
            'region2.nombre as region2',
            \DB::raw("count(stock.id) as cantidad")
            ]);

        return $query;
    }

    private function selectProvincias($query)
    {
        $query = $query->select([
            'provincia.nombre as provincia',
            \DB::raw("count(provincia_id) as cantidad")
            ]);

        return $query;
    }

    private function selectSubCategorias($query)
    {
        $query = $query->select([
            \DB::raw("count(subcategoria1_id) as cantidad"),
            'subcategoria2.tipo as subcategoria'
            ]);

        return $query;
    }

    private function formatCategories($items)
    {
        $categorias = Subcategoria1::all();
        $regiones = Region1::all();
        $muebles = [];
        foreach($regiones as $region){
            foreach($categorias as $categoria){
                $itemExist = false;
                foreach($items as $item){
                    if($item->region == $region->nombre && $item->categoria == $categoria->tipo) {
                        $muebles[] = $item;
                        $itemExist = true;
                        break;
                    }
                }
                if(!$itemExist){
                    $newItem = new \stdClass();
                    $newItem->region = $region->nombre;
                    $newItem->categoria = $categoria->tipo;
                    $newItem->cantidad = 0;
                    $muebles[] = $newItem;
                }
            }
        }
        return $muebles;
    }

    private function formatMueblesPorCliente($items)
    {
        $total = 0;
        foreach($items as $item){
            $total += $item->total_muebles;
        }
        foreach($items as &$item){
            $item->porcentaje = round(($item->total_muebles/$total)*100, 2);
        }
        return $items;
    }

    private function formatRegion($items)
    {
        $total = 0;
        foreach($items as $item){
            $total += $item->cantidad;
        }
        foreach($items as &$item){
            $item->porcentaje = round(($item->cantidad/$total)*100, 2);
        }
        return $items;
    }

    private function formatProvincias($items)
    {
        $provincias = Provincia::all();
        $muebles = [];
        foreach($provincias as $provincia){
            $itemExist = false;
            foreach($items as $item){
                if($item->provincia == $provincia->nombre) {
                    $muebles[] = $item;
                    $itemExist = true;
                    break;
                }
            }
            if(!$itemExist){
                $newItem = new \stdClass();
                $newItem->provincia = $provincia->nombre;
                $newItem->cantidad = 0;
                $muebles[] = $newItem;
            }
        }
        return collect($muebles);
    }

    private function formatSubCategories($items)
    {
        $subCategorias = Subcategoria2::all();
        $muebles = [];
        foreach($subCategorias as $subCategoria){
            $itemExist = false;
            foreach($items as $item){
                if($item->subcategoria == $subCategoria->tipo) {
                    $muebles[] = $item;
                    $itemExist = true;
                    break;
                }
            }
            if(!$itemExist){
                $newItem = new \stdClass();
                $newItem->subcategoria = $subCategoria->tipo;
                $newItem->cantidad = 0;
                $muebles[] = $newItem;
            }
        }
        return $muebles;
    }
}