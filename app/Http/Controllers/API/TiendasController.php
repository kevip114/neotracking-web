<?php


namespace App\Http\Controllers\API;

use App\Models\Tienda;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\TipoTienda;
use App\Repositories\TiendasRepository;
use Symfony\Component\HttpFoundation\Request;

class TiendasController extends Controller{

    protected $tiendaRepository;


    public function __construct(TiendasRepository $tiendaRepository){
        $this->tiendaRepository = $tiendaRepository;
    }

    public function index(Request $request){
        $sin_stock = $request->get('sin_stock');
        if(isset($sin_stock) && $sin_stock==1){
            return $this->tiendaRepository->getTiendasSinStock($request);
        }else if(isset($sin_stock) && $sin_stock==0){
            return $this->tiendaRepository->getTiendasConStock($request);
        }
        return Tienda::all();
    }

    public function store(Request $request){
        return $this->tiendaRepository->store($request);
    }

    public function getTipo(){
        return TipoTienda::all();
    }

    public function getTiendasSinStock(Request $request)
    {
        $status   = $request->get('status');

        return $this->$tiendaRepository->getTiendasSinStock(null, null, $status);
    }

}