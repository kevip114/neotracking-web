<?php

namespace App\Http\Controllers\API\reportes;

use App\Repositories\reportes\ReporteMueblesRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReporteMueblesController extends Controller
{
    protected $reporteMueblesRepository;

    public function __construct(ReporteMueblesRepository $reporteMueblesRepository)
    {
        $this->reporteMueblesRepository = $reporteMueblesRepository;
    }

    public function getMueblesPorCliente(Request $request)
    {
        $from = (new Carbon($request->get('from')))->hour(6)->minute(0)->second(0);
        $to   = (new Carbon($request->get('to')))->addDay()->hour(6)->minute(0)->second(0);
        $status   = $request->get('status');
        $region1 = $request->get('region1');

        return $this->reporteMueblesRepository->getMueblesPorCliente($from, $to, $status, $region1);

    }

    public function getCategorias(Request $request)
    {
        $from = (new Carbon($request->get('from')))->hour(6)->minute(0)->second(0);
        $to   = (new Carbon($request->get('to')))->addDay()->hour(6)->minute(0)->second(0);
        $status   = $request->get('status');

        return $this->reporteMueblesRepository->getCategorias($from, $to, $status);

    }

    public function getSubCategorias(Request $request)
    {
        $from = (new Carbon($request->get('from')))->hour(6)->minute(0)->second(0);
        $to   = (new Carbon($request->get('to')))->addDay()->hour(6)->minute(0)->second(0);
        $status   = $request->get('status');

        return $this->reporteMueblesRepository->getSubCategorias($from, $to, $status);

    }

    public function getProvincias(Request $request)
    {
        $from = (new Carbon($request->get('from')))->hour(6)->minute(0)->second(0);
        $to   = (new Carbon($request->get('to')))->addDay()->hour(6)->minute(0)->second(0);
        $status   = $request->get('status');
        $provincias =strtoupper($request->get('provincias'));
        return $this->reporteMueblesRepository->getProvincias($from, $to, $status, $provincias);

    }

    public function getRegion1(Request $request)
    {
        $from = (new Carbon($request->get('from')))->hour(6)->minute(0)->second(0);
        $to   = (new Carbon($request->get('to')))->addDay()->hour(6)->minute(0)->second(0);
        $status   = $request->get('status');

        return $this->reporteMueblesRepository->getRegion1($from, $to, $status);

    }

    public function getRegion2(Request $request)
    {
        $from = (new Carbon($request->get('from')))->hour(6)->minute(0)->second(0);
        $to   = (new Carbon($request->get('to')))->addDay()->hour(6)->minute(0)->second(0);
        $status   = $request->get('status');

        return $this->reporteMueblesRepository->getRegion2($from, $to, $status);

    }
}
