<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\TrackImagen;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class ResizeImageController extends Controller{

    public function resize(){
        $images = TrackImagen::all();
        $path = public_path() . '/images/resized-images';
        if(!File::exists($path)){
            File::makeDirectory($path, 777);
        }
        foreach($images as $image){
            $pathImage = public_path() . '/images/' . $image->name;
            $img = Image::make($pathImage);
            if(!file_exists($path . '/60h' . $image->name)) {
                $img->resize(null, 60, function ($constraint) {
                    $constraint->aspectRatio();
                    //$constraint->upsize();
                });
                $img->save($path . '/60h' . $image->name, 60);
            }
        }

    }

}