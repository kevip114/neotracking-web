(function() {
  'use strict';

  angular
    .module('neotrackingWeb')
    .config(config);

  /** @ngInject */
  function config($logProvider, toastrConfig, $httpProvider, $routeProvider, $authProvider, usSpinnerConfigProvider) {
    // Enable log
    $logProvider.debugEnabled(true);

    // Set options third-party lib
    toastrConfig.allowHtml = true;
    toastrConfig.timeOut = 3000;
    toastrConfig.positionClass = 'toast-top-right';
    toastrConfig.preventDuplicates = false;
    toastrConfig.progressBar = true;
      $authProvider.loginUrl = "http://localhost:8000/auth_login";
      //$authProvider.loginUrl = "http://lg.neoprojects.com.pe/auth_login";
      usSpinnerConfigProvider.setTheme('smallRed', {color: 'red', radius: 6});

      Chart.pluginService.register({
          beforeRender: function (chart) {
              if (chart.config.options.showAllTooltips) {
                  console.log(chart.options.tooltips);
                  // create an array of tooltips
                  // we can't use the chart tooltip because there is only one tooltip per chart
                  chart.pluginTooltips = [];
                  chart.config.data.datasets.forEach(function (dataset, i) {
                      chart.getDatasetMeta(i).data.forEach(function (sector, j) {
                          chart.pluginTooltips.push(new Chart.Tooltip({
                              _chart: chart.chart,
                              _chartInstance: chart,
                              _data: chart.data,
                              _options: chart.options.tooltips,
                              _active: [sector]
                          }, chart));
                      });
                  });

                  // turn off normal tooltips
                  chart.options.tooltips.enabled = false;
              }
          },
          afterDraw: function (chart, easing) {
              if (chart.config.options.showAllTooltips) {
                  // we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
                  if (!chart.allTooltipsOnce) {
                      if (easing !== 1)
                          return;
                      chart.allTooltipsOnce = true;
                  }

                  // turn on tooltips
                  chart.options.tooltips.enabled = true;
                  Chart.helpers.each(chart.pluginTooltips, function (tooltip) {
                      tooltip.initialize();
                      tooltip.update();
                      // we don't actually need this since we are not animating tooltips
                      tooltip.pivot();
                      tooltip.transition(easing).draw();
                  });
                  chart.options.tooltips.enabled = false;
              }
          }
      })
  }

})();
