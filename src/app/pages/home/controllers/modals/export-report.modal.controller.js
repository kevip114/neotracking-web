(function() {
    'use strict';

    angular
        .module('neotrackingWeb')
        .controller('ExportReportModalController', ExportReportModalController);

    /** @ngInject */
    ExportReportModalController.$inject = [
        'excelReport',
        '$mdDialog'
    ];

    function ExportReportModalController(excelReport, $mdDialog) {
        var vm = this;
        vm.cancel = cancel;
        vm.excelReport = excelReport;

        function cancel(){
            $mdDialog.cancel();
        }


    }
})();
