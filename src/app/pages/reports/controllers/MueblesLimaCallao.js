(function() {
    'use strict';

    angular
        .module('neotrackingWeb')
        .controller('MueblesLimaCallaoController', MueblesLimaCallaoController);

    /** @ngInject */
    MueblesLimaCallaoController.$inject = ['MueblesPorProvincias'];

    function MueblesLimaCallaoController(MueblesPorProvincias) {
        var vm = this;

        vm.labels = [];
        vm.data = [];
        vm.generar = generar;
        vm.status = "";

        MueblesPorProvincias.all({provincias: 'lima,callao'}).$promise.then(successGet,errorGet);

        vm.generar();
        vm.options={
            legend: {
                display: true,
                position: 'left'
            },
            showAllTooltips: true,
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        var allData = data.datasets[tooltipItem.datasetIndex].data;
                        var tooltipLabel = data.labels[tooltipItem.index];
                        var tooltipData = allData[tooltipItem.index];
                        var total = 0;
                        for (var i in allData) {
                            total += allData[i];
                        }
                        var tooltipPercentage = Math.round((tooltipData / total) * 100);
                        return tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
                    }
                }
            }
        };

        function errorGet(err){

        }

        function generar(){
            MueblesPorProvincias.all({provincias: 'lima,callao'}).$promise.then(successGet,errorGet);
        }

        function successGet(muebles){
            vm.labels = [];
            vm.data = [];
            vm.muebles = muebles;
            for(var i=0; i<muebles.length;i++){
                vm.labels.push(muebles[i].provincia);
                vm.data.push(muebles[i].cantidad);
            }

        }

    }
})();
