(function() {
    'use strict';

    angular
        .module('neotrackingWeb')
        .controller('ReporteCategoriasController', ReporteCategoriasController);

    /** @ngInject */
    ReporteCategoriasController.$inject = ['MueblesPorCategorias'];

    function ReporteCategoriasController(MueblesPorCategorias) {
        var vm = this;

        vm.labels = [];
        vm.data = [];
        vm.generar = generar;
        vm.options= {
            legend: {
                display: true
            }
        };
        vm.series = ["LIMA", "PROVINCIA"];
        vm.status = "";

        MueblesPorCategorias.all().$promise.then(successGet,errorGet);

        vm.generar();

        function errorGet(err){

        }

        function generar(){
            MueblesPorCategorias.all().$promise.then(successGet,errorGet);
        }
        function successGet(muebles){
            var region1 = [],
                region2 = [];
            vm.labels = [];
            vm.data = [];
            vm.muebles = muebles;

            for(var i=0; i<muebles.length/2;i++){
                vm.labels.push(muebles[i].categoria);
                region1.push(muebles[i].cantidad);
            }
            vm.data.push(region1);
            for(var j=muebles.length/2; j<muebles.length;j++){
                region2.push(muebles[j].cantidad);
            }
            vm.data.push(region2);
            console.log(region1);
            console.log(region2);
        }
    }
})();
