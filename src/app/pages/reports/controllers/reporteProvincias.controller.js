(function() {
    'use strict';

    angular
        .module('neotrackingWeb')
        .controller('ReporteProvinciasController', ReporteProvinciasController);

    /** @ngInject */
    ReporteProvinciasController.$inject = ['MueblesPorProvincias'];

    function ReporteProvinciasController(MueblesPorProvincias) {
        var vm = this;

        vm.labels = [];
        vm.data = [];
        vm.generar = generar;
        vm.status = "";

        MueblesPorProvincias.all().$promise.then(successGet,errorGet);

        vm.generar();

        function errorGet(err){

        }

        function generar(){
            MueblesPorProvincias.all().$promise.then(successGet,errorGet);
        }
        function successGet(muebles){
            vm.labels = [];
            vm.data = [];
            vm.muebles = muebles;
            for(var i=0; i<muebles.length;i++){
                vm.labels.push(muebles[i].provincia);
                vm.data.push(muebles[i].cantidad);
            }

        }
    }
})();
