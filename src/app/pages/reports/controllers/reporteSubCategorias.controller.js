(function() {
    'use strict';

    angular
        .module('neotrackingWeb')
        .controller('ReporteSubCategoriasController', ReporteSubCategoriasController);

    /** @ngInject */
    ReporteSubCategoriasController.$inject = ['MueblesPorSubCategorias'];

    function ReporteSubCategoriasController(MueblesPorSubCategorias) {
        var vm = this;

        vm.labels = [];
        vm.data = [];
        vm.generar = generar;
        vm.status = "";

        MueblesPorSubCategorias.all().$promise.then(successGet,errorGet);

        vm.generar();

        function errorGet(err){

        }

        function generar(){
            MueblesPorSubCategorias.all().$promise.then(successGet,errorGet);
        }
        function successGet(muebles){
            vm.labels = [];
            vm.data = [];
            vm.muebles = muebles;
            for(var i=0; i<muebles.length;i++){
                vm.labels.push(muebles[i].subcategoria);
                vm.data.push(muebles[i].cantidad);
            }

        }
    }
})();
