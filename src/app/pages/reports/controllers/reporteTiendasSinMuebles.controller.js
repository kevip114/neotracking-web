(function() {
    'use strict';

    angular
        .module('neotrackingWeb')
        .controller('ReporteTiendasSinMueblesController', ReporteTiendasSinMueblesController);

    /** @ngInject */
    ReporteTiendasSinMueblesController.$inject = ['Tienda'];

    function ReporteTiendasSinMueblesController(Tienda) {
        var vm = this;
        vm.pageChangeHandler = pageChangeHandler;
        vm.disablePaginator = false;

        Tienda.all().$promise.then(successGetTiendas, function(){});

        function pageChangeHandler(newPageNumber, e){
            if(!vm.disablePaginator){
                vm.disablePaginator = true;
                Tienda.paginate({
                    page: newPageNumber,
                    sin_stock: 1
                }).$promise.then(successGet,errorGet);
            }
        }

        function errorGet(err){
            vm.disablePaginator = false;
            console.log(err);
        }

        function successGet(res){
            vm.disablePaginator = false;
            vm.muebles = res;
        }

        function successGetTiendas(tiendas){
            vm.tiendas = tiendas;
            Tienda.paginate({//tiendas sin stock
                page: 1,
                sin_stock: 1
            }).$promise.then(function(resp){
                    vm.disablePaginator = false;
                    vm.muebles = resp;
                    vm.porcentajeTiendasSinStock = (vm.muebles.total/vm.tiendas.length)*100;
                    Tienda.paginate({//tiendas con stock
                        page: 1,
                        sin_stock: 0
                    }).$promise.then(function(res){
                            vm.mueblesConStock = res;
                            vm.porcentajeTiendasConStock = (vm.mueblesConStock.total/vm.tiendas.length)*100;
                        },function error(){});
                },errorGet);
        }

    }
})();
