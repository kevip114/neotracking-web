(function() {
    'use strict';

    angular
        .module('neotrackingWeb')
        .controller('MueblesPorClienteController', MueblesPorClienteController);

    /** @ngInject */
    MueblesPorClienteController.$inject = ['MueblesPorCliente'];

    function MueblesPorClienteController(MueblesPorCliente) {
        var vm = this;

        vm.labels = [];
        vm.data = [];
        vm.estados = [
            {
                nombre: "Todos",
                tipo: ""
            },
            {
                nombre: "Alta",
                tipo: "alta"
            },
            {
                nombre: "Baja",
                tipo: "baja"
            },
            {
                nombre: "Pendientes de alta",
                tipo: "pendiente_alta"
            },
            {
                nombre: "Pendientes de baja",
                tipo: "pendiente_baja"
            }
        ];
        vm.generar = generar;
        vm.options={
            legend: {
                display: true,
                position: 'left'
            },
            showAllTooltips: false,
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        var allData = data.datasets[tooltipItem.datasetIndex].data;
                        var tooltipLabel = data.labels[tooltipItem.index];
                        var tooltipData = allData[tooltipItem.index];
                        var total = 0;
                        for (var i in allData) {
                            total += allData[i];
                        }
                        var tooltipPercentage = Math.round((tooltipData / total) * 100);
                        return tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
                    }
                }
            }
        };
        vm.status = "";

        MueblesPorCliente.all().$promise.then(successGet,errorGet);

        function errorGet(err){

        }

        function generar(){
            MueblesPorCliente.all({status: vm.status}).$promise.then(successGet,errorGet);
        }

        function successGet(muebles){
            vm.labels = [];
            vm.data = [];
            vm.muebles = muebles;
            muebles.forEach(function(mueble, key){
                vm.labels.push(mueble.retail);
                vm.data.push(mueble.total_muebles);
            });
        }

        function groupByCategories(muebles){
            //@TODO agrupar muebles por categorias
        }
    }
})();
