(function() {
    'use strict';

    angular
        .module('neotrackingWeb')
        .controller('FurnitureHistoryController', FurnitureHistoryController);

    /** @ngInject */

    FurnitureHistoryController.$inject = [
        '$mdDialog',
        'furnitureHistory',
        'FurnitureHistory',
        'Stock',
        '$timeout',
        '$scope'
    ];

    function FurnitureHistoryController($mdDialog, furnitureHistory, FurnitureHistory, Stock, $timeout, $scope) {
        var vm = this,
            timeoutPromise;
        vm.pageChangeHandler = pageChangeHandler;
        vm.furnitureHistory = furnitureHistory;
        vm.disablePaginator = false;
        init();

        function init(){
            formatJson();
        }

        function formatJson(){
            angular.forEach(vm.furnitureHistory.data, function(value, key){
                angular.forEach(value, function(v, k){
                    if(k == 'user' || k == 'new_status' || k == 'stock_id' || k == 'categoria_id' || k == 'subcategoria1_id' || k == 'subcategoria2_id' || k == 'tienda_id'){
                        vm.furnitureHistory.data[key][k] = (v != "") ? JSON.parse(v) : v;
                    }
                });
            });
        }

        function pageChangeHandler(newPageNumber, e){
            if(!vm.disablePaginator){
                vm.disablePaginator = true;
                FurnitureHistory.all({
                    page: newPageNumber,
                    keyword:vm.keyword
                }).$promise.then(successGetStock,errorGetStock);
            }
        }

        function errorGetStock(){
            vm.disablePaginator = false;
        }

        /**
         * This method makes an http call to users for every keyup and there's a delay of 800ms for every request,
         * if a new http request is called before 800ms the last request is canceled
         * @param keyword
         * @param oldKeyword
         */
        function searchFurniture(keyword, oldKeyword){
            //@TODO Create directive so you dont have to reuse this code
            $timeout.cancel(timeoutPromise);
            timeoutPromise = $timeout(function() {
                if(typeof keyword !== 'undefined'){
                    if(keyword.length>=2){
                        Stock.paginate({
                            page: 1,
                            keyword:vm.keyword
                        }).$promise.then(successGetStock,errorGetStock);
                    }else if(typeof oldKeyword!=="undefined") {
                        if(keyword.length<oldKeyword.length && keyword.length<2) {
                            Stock.paginate({
                                page: 1
                            }).then(successGetStock,errorGetStock);
                        }
                    }
                }
            }, 500);
        }


        function successGetStock(response){
            vm.disablePaginator = false;
            vm.furnitureHistory = response;
            formatJson();
        }

    }
})();
