(function() {
    'use strict';

    angular
        .module('neotrackingWeb')
        .controller('FurnitureDetailController', FurnitureDetailController);

    /** @ngInject */

    FurnitureDetailController.$inject = [
        '$stateParams',
        '$http',
        'API_URL',
        'stock'
    ];

    function FurnitureDetailController($stateParams, $http, API_URL, stock) {
        var vm = this;
        vm.codigo = $stateParams.codigo;
        vm.stock = stock;

        $http.get(API_URL+'stock/'+$stateParams.codigo+'/historial').then(
            function(res){
                vm.tracks = res.data;
            }
        );

    }
})();
