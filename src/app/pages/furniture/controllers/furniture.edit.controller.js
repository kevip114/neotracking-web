(function() {
    'use strict';

    angular
        .module('neotrackingWeb')
        .controller('FurnitureEditController', FurnitureEditController);

    /** @ngInject */

    FurnitureEditController.$inject = [
        '$mdDialog',
        'Categoria',
        'Subcategoria1',
        'Subcategoria2',
        'Stock',
        'toastr',
        '$state',
        '$http',
        'API_URL',
        'stock',
        'Proveedor'
    ];

    function FurnitureEditController(
        $mdDialog,
        Categoria,
        Subcategoria1,
        Subcategoria2,
        Stock,
        toastr,
        $state,
        $http,
        API_URL,
        stock,
        Proveedor) {

        var vm = this;
        vm.areas = Categoria.all();
        vm.categorias = Subcategoria1.all();
        vm.proveedor = {};
        vm.proveedores = Proveedor.all();
        vm.subcategorias = Subcategoria2.all();
        vm.stock = stock;
        vm.stock_status = vm.stock.status;
        vm.state = $state.current.name;
        vm.submit = submit;
        vm.selectedItemChange = selectedItemChange;

        init();

        function init(){
            getLatTrack();
        }

        function getLatTrack(){
            $http.get(API_URL+'stock/'+vm.stock.codigo+'/last-track').then(
                function success(res){
                    vm.track = res.data;
                }
            );
        }

        function selectedItemChange(item){
            //@TODO implement selectedItemChange()
        }
        function submit(e){
            e.preventDefault();
            var searchProveedor = vm.searchProveedor==null?vm.searchProveedor:(vm.searchProveedor).toUpperCase();
            vm.stock.precio=parseFloat(vm.stock.precio);

            vm.stock.proveedor= (vm.stock.proveedor != null) ? vm.stock.proveedor : searchProveedor;
            vm.stock.sugerencia_baja_antes_de_alta = vm.sbada ? 1 : 0;
            vm.stock.status = vm.stock_status;
            vm.stock.$update(function(res){
                $state.go('index.furniture');
                toastr.success('Se editó mobiliario satisfactoriamente');

            }, function error(err){
                console.log(err);
                if(typeof err.data.errors !== 'undefined'){
                    for(var i in err.data.errors){
                        for(var j in err.data.errors[i]){
                            console.log(err.data.errors[i][j]);
                            toastr.error(err.data.errors[i][j]);
                        }
                    }
                }

            });
        }
    }
})();
