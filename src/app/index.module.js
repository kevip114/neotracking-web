(function() {
  'use strict';

  angular
    .module('neotrackingWeb', [
          'authService',
          'ngCookies',
          'ngSanitize',
          'ngMessages',
          'ngAria',
          'ngResource',
          'ngRoute',
          'ngMaterial',
          'ui.router',
          'toastr',
          'satellizer',
          'ngScrollbars',
          'ngMap',
          'angularSpinner',
          'datatables',
          'datatables.buttons',
          'angularUtils.directives.dirPagination',
          'ui.utils.masks',
          'chart.js',
          'angular-loading-bar'
      ]);

})();
