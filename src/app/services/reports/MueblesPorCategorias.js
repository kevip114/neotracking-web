(function() {
    'use strict';

    angular
        .module('neotrackingWeb')
        .factory('MueblesPorCategorias', ['$resource', 'API_URL', function($resource, API_URL) {
            return $resource(API_URL+'reportes/graficos/barra-horizontal/muebles-categorias', {}, {
                all    : {method: 'GET', isArray: true}
            });
        }]);

})();
