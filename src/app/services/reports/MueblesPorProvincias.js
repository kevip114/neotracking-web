(function() {
    'use strict';

    angular
        .module('neotrackingWeb')
        .factory('MueblesPorProvincias', ['$resource', 'API_URL', function($resource, API_URL) {
            return $resource(API_URL+'reportes/graficos/barra/muebles-provincias', {}, {
                all    : {method: 'GET', isArray: true}
            });
        }]);

})();

