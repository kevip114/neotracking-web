(function() {
    'use strict';

    angular
        .module('neotrackingWeb')
        .factory('MueblesPorRegion1', ['$resource', 'API_URL', function($resource, API_URL) {
            return $resource(API_URL+'reportes/graficos/pie/muebles-region1', {}, {
                all    : {method: 'GET', isArray: true}
            });
        }]);

})();
