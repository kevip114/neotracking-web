(function() {
    'use strict';

    angular
        .module('neotrackingWeb')
        .factory('MueblesPorCliente', ['$resource', 'API_URL', function($resource, API_URL) {
            return $resource(API_URL+'reportes/graficos/pie/muebles-cliente', {}, {
                all    : {method: 'GET', isArray: true}
            });
        }]);

})();
