(function() {
    'use strict';

    angular
        .module('neotrackingWeb')
        .factory('MueblesPorSubCategorias', ['$resource', 'API_URL', function($resource, API_URL) {
            return $resource(API_URL+'reportes/graficos/barra/muebles-subcategorias', {}, {
                all    : {method: 'GET', isArray: true}
            });
        }]);

})();

