(function() {
    'use strict';

    angular
        .module('neotrackingWeb')
        .factory('MueblesPorRegion2', ['$resource', 'API_URL', function($resource, API_URL) {
            return $resource(API_URL+'reportes/graficos/pie/muebles-region2', {}, {
                all    : {method: 'GET', isArray: true}
            });
        }]);

})();
