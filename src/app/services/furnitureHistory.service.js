(function() {
    'use strict';

    angular
        .module('neotrackingWeb')
        .factory('FurnitureHistory', ['$resource', 'API_URL', function($resource, API_URL) {
            return $resource(API_URL+'stock-log', {}, {
                all    : {method: 'GET', isArray: false}
            });
        }]);

})();
